#!/usr/bin/python3


import webapp

formulario = """
    <br/><br/>
    <form action="" method = "POST">
    <input type="text" name="name" value="">
    <input type="submit" value="Enviar">
    </form>
"""


class contentPutPostApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Diccionario con el contenido para los recursos disponibles
    content = {'/': 'Inicio',
               '/page': 'A page',
               '/frio': 'Hace frío',
               '/calor': 'Hace calor',
               '/lluvia': 'Está lloviendo',
               '/sol': 'El sol brilla intensamente'
               }

    def parse(self, request):
        """Parse the received request, extracting the relevant information."""

        metodo, recurso, _ = request.split(' ', 2)  # Trocea los dos primeros
        # espacios en blanco y guarda las dos primeras partes, el resto lo deshecha

        try:
            cuerpo = request.split('\r\n\r\n', 1)[1]  # Trocea una sola vez por \r\n\r\n y
            # guarda el final para obtener el cuerpo
        except IndexError:
            cuerpo = ""
        return metodo, recurso, cuerpo

    def process(self, resourceName):
        """Process the relevant elements of the request."""

        metodo, recurso, cuerpo = resourceName
        print('El metodo es: ' + metodo)
        print('El recurso es: ' + recurso)
        print('El cuerpo es: ' + cuerpo)

        if metodo == "GET":
            try:
                valor_guardado = self.content[recurso]
                print("Contenido en dicc: " + valor_guardado)
            except KeyError:
                print("El recurso " + recurso + " no esta guardado.")
        if metodo == "PUT":  # En este caso machaca el valor si se repite el recurso
                self.content[recurso] = cuerpo  # Guarda el cuerpo en el diccionario con clave el recurso
        elif metodo == "POST":  # Guarda lo introducido en el formulario
            # Si el recurso no estaba, se guarda ese contenido en una nueva entrada en el diccionario para ese recurso
            # Si el recurso ya existe, actualiza su contenido en el diccionario
            self.content[recurso] = cuerpo.split('=')[1].replace('+', ' ')  # con el metodo replace se sustituyen los
            # caracteres + resultantes de escribir varias palabras espaciadas en el cuerpo (formulario) por espacios

        print(self.content)

        if recurso in self.content.keys():
            httpCode = "200 OK"
            htmlBody = "<html><body>" + self.content[recurso] \
                + formulario + "</body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = ("<html><body>El recurso " + recurso + " no forma parte de los contenidos."
                        + formulario + "</body></html>")
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentPutPostApp("localhost", 1234)
